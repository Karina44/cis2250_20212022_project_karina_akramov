package info.hccis.tutoringapp_student.adapter;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import info.hccis.tutoringapp_student.R;
import info.hccis.tutoringapp_student.entity.Student;

public class CustomAdapterStudent extends RecyclerView.Adapter<CustomAdapterStudent.StudentViewHolder> {

    private List<Student> studentArrayList;

    public CustomAdapterStudent(List<Student> studentBOArrayList) {
        this.studentArrayList = studentBOArrayList;
    }

    ;

    @NonNull
    @Override
    public StudentViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_view_student, parent, false);
        return new StudentViewHolder(itemView);


    }

    @Override
    public void onBindViewHolder(@NonNull StudentViewHolder holder, int position) {

        String firstName = "" + studentArrayList.get(position).getFirstName();
        String lastName = "" + studentArrayList.get(position).getLastName();
        String phone = "" + studentArrayList.get(position).getPhoneNumber();
        String email = "" + studentArrayList.get(position).getEmailAddress();
        String programCode = "" + studentArrayList.get(position).getProgramCode();
        holder.firstName.setText(firstName);
        holder.lastName.setText(lastName);
        holder.phone.setText(phone);
        holder.email.setText(email);
        holder.programCode.setText(programCode);

    }

    @Override
    public int getItemCount() {
        return studentArrayList.size();
    }

    public class StudentViewHolder extends RecyclerView.ViewHolder {
        private TextView firstName;
        private TextView lastName;
        private TextView phone;
        private TextView email;
        private TextView programCode;

        public StudentViewHolder(final View view) {
            super(view);
            firstName = view.findViewById(R.id.textViewFirstNameListItem);
            lastName = view.findViewById(R.id.textViewLastNameListItem);
            phone = view.findViewById(R.id.textViewPhoneListItem);
            email = view.findViewById(R.id.textViewEmailListItem);
            programCode = view.findViewById(R.id.textViewProgramCodeListItem);
        }
    }


}
