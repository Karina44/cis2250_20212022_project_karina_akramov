package info.hccis.tutoringapp_student.api;
import android.content.SharedPreferences;
import android.util.Log;

import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.preference.PreferenceManager;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

import info.hccis.tutoringapp_student.MainActivity;
import info.hccis.tutoringapp_student.R;

import info.hccis.tutoringapp_student.ViewStudentFragment;
import info.hccis.tutoringapp_student.entity.Student;
import info.hccis.tutoringapp_student.entity.StudentContent;
import info.hccis.tutoringapp_student.entity.StudentViewModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
public class ApiWatcher extends Thread {

  // public static final String API_BASE_URL = "http://localhost:8081/api/StudentService/";
   public static final String API_BASE_URL = "http://10.0.2.2:8081/api/StudentService/";

    private int lengthLastCall = -1;  //Number of rows returned


    //The activity is passed in to allow the runOnUIThread to be used.
    private FragmentActivity activity = null;

    public void setActivity(FragmentActivity activity) {
        this.activity = activity;
    }

    @Override
    public void run() {
        super.run();
        try {
            do {

                //************************************************************************
                // A lot of this code is duplicated from the TicketOrderContent class.  It will
                // access the api and if if notes that the number of orders has changed, will
                // notify the view order fragment that the data is changed.
                //************************************************************************

                Log.d("Karina api", "running");

                //Use Retrofit to connect to the service
                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl(ApiWatcher.API_BASE_URL)
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();

                JsonStudentApi jsonStudentApi = retrofit.create(JsonStudentApi.class);

                //Create a list of ticket orders.
                Call<List<Student>> call = jsonStudentApi.getStudents();

                StudentViewModel studentViewModel = new ViewModelProvider(activity).get(StudentViewModel.class);

                call.enqueue(new Callback<List<Student>>() {

                    @Override
                    public void onResponse(Call<List<Student>> call, Response<List<Student>> response) {

                        //See if we can get the view model.  This contains the list of orders
                        //which is used to populate the recycler view on the list fragment.
                        Log.d("Karina api", "found Student view model. size=" + studentViewModel.getStudents().size());


                        if (!response.isSuccessful()) {
                            Log.d("Karina api", "Karina not successful response from rest for Students Code=" + response.code());

                            //******************************************************************************************
                            // Using the default shared preferences.  Using the application context - may want to access the
                            // shared prefs from other activities.
                            //******************************************************************************************

                            loadFromRoomIfPreferred(studentViewModel);
                            lengthLastCall = -1; //Indicate couldn't load from api will trigger reload next time

                        } else {
                            //Take the list and pass to constructor of ArrayList to convert it.
                            ArrayList<Student> studentsTemp = new ArrayList(response.body()); //note gson will be used implicitly
                            int lengthThisCall = studentsTemp.size();

                            Log.d("Karina api", "back from api, size=" + lengthThisCall);


                            if (lengthLastCall == -1) {
                                //first time - don't notify
                                lengthLastCall = lengthThisCall;
                                //******************************************************************
                                //Note here.  The recycler view is using the ArrayList from this
                                //ticketOrderViewModel class.  We will take the ticket orders obtained
                                //from the api and add refresh the list .  Will then notify the
                                //recyclerview that things have changed.
                                //******************************************************************
                                studentViewModel.setStudents(studentsTemp); //Will addAll
                                Log.d("Karina api ", "First load of Students from the api");

                                //**********************************************************************
                                // This method will allow a call to the runOnUiThread which will be allowed
                                // to interact with the ui components of the app.
                                //**********************************************************************
                                activity.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Log.d("Karina api", "trying to notify adapter that things have changed");
                                        ViewStudentFragment.notifyDataChanged("Found more rows");
                                    }
                                });

                                //******************************************************************
                                //Save latest orders in the database.
                                //******************************************************************
                                StudentContent.reloadStudentsInRoom(studentsTemp);




                            } else if (lengthThisCall != lengthLastCall) {
                                //******************************************************************
                                //data has changed
                                //******************************************************************
                                Log.d("Karina api", "Data has changed");
                                lengthLastCall = lengthThisCall;
                                studentViewModel.getStudents().clear();
                                studentViewModel.getStudents().addAll(studentsTemp);

                                //**********************************************************************
                                // This method will allow a call to the runOnUiThread which will be allowed
                                // to interact with the ui components of the app.
                                //**********************************************************************
                                activity.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Log.d("Karina api", "trying to notify adapter that things have changed");
                                        Log.d("Karina api", "Also using method to send a notification to the user");
                                        ViewStudentFragment.notifyDataChanged("Update - the data has changed", activity, MainActivity.class);
                                    }
                                });

                                //******************************************************************
                                //Save latest orders in the database.
                                //******************************************************************
                                StudentContent.reloadStudentsInRoom(studentsTemp);

                            } else {
                                //*******************************************************************
                                // Same number of rows so don't bother updating the list.
                                //*******************************************************************
                                Log.d("Karina api", "Data has not changed");
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<List<Student>> call, Throwable t) {

                        //**********************************************************************************
                        // If the api call failed, give a notification to the user.
                        //**********************************************************************************
                        Log.d("Karina api", "api call failed");
                        Log.d("Karina api", t.getMessage());

                        //******************************************************************************************
                        // Using the default shared preferences.  Using the application context - may want to access the
                        // shared prefs from other activities.
                        //******************************************************************************************

                        lengthLastCall = -1; //Indicate couldn't load from api will trigger reload next time
                        loadFromRoomIfPreferred(studentViewModel);

                    }
                });

                //***********************************************************************************
                // Sleep so not checking all the time
                //***********************************************************************************
                final int SLEEP_TIME = 10000;
                Log.d("Karina Sleep", "Sleeping for " + (SLEEP_TIME / 1000) + " seconds");
                Thread.sleep(SLEEP_TIME); //Check api every 10 seconds

            } while (true);
        } catch (InterruptedException e) {
            Log.d("Karina api", "Thread interrupted.  Stopping in the thread.");
        }
    }

    /**
     * Check the shared preferences and load from the db if the setting is set to do such a thing.
     * @param studentViewModel
     * @since 20220211
     * @author Karina
     */
    public void loadFromRoomIfPreferred(StudentViewModel studentViewModel) {
        Log.d("Karina room","Check to see if should load from room");
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(activity.getApplicationContext());
        boolean loadFromRoom = sharedPref.getBoolean(activity.getString(R.string.preference_load_from_room), true);
        Log.d("Karina room","Load from Room="+loadFromRoom);
        if (loadFromRoom) {
            List<Student> testList = MainActivity.getMyAppDatabase().studentDAO().selectAllStudents();
            Log.d("Karina room","Obtained Student from the db: "+testList.size());
            studentViewModel.setStudents(testList); //Will add all ticket orders

            //**********************************************************************
            // This method will allow a call to the runOnUiThread which will be allowed
            // to interact with the ui components of the app.
            //**********************************************************************
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Log.d("Karina api", "trying to notify adapter that things have changed");
                    ViewStudentFragment.notifyDataChanged("Found more rows");
                }

            });

        }
    }

}
