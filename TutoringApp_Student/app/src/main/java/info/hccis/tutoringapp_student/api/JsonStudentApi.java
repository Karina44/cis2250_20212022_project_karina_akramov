package info.hccis.tutoringapp_student.api;
import java.util.List;
import java.util.Map;

import info.hccis.tutoringapp_student.entity.Student;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface JsonStudentApi {

    /**
     * This abstract method to be created to allow retrofit to get list of ticket orders
     * @return List of ticket orders
     * @since 20220202
     * @author Karina (with help from the retrofit research!).
     * @updated Karina A
     */

    @GET("student")
    Call<List<Student>> getStudents();
    @POST("student")
    Call<Student> createStudent(@Body Student student);

}