package info.hccis.tutoringapp_student;

import android.app.Activity;
import android.os.Bundle;

import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;


import info.hccis.tutoringapp_student.util.CisSocialMediaUtil;

public class SocialMedia extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_social_media);

        EditText editTextMessage = findViewById(R.id.editTextMessage);

        Button buttonSendMessage = findViewById(R.id.buttonSendMessage);
        Activity thisActivity = this;
        buttonSendMessage.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                String message = editTextMessage.getText().toString();

                //******************************************************************************
                //Social Media
                //******************************************************************************

                startActivity(CisSocialMediaUtil.shareOnSocialMedia(thisActivity, CisSocialMediaUtil.PACKAGE_TWITTER, "App", message));

            }
        });
    }
}