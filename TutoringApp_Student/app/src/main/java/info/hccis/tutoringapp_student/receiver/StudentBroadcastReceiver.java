package info.hccis.tutoringapp_student.receiver;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;
public class StudentBroadcastReceiver extends BroadcastReceiver{
    public StudentBroadcastReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {

        Toast.makeText(context, "Action: " + intent.getAction(), Toast.LENGTH_SHORT).show();
        Log.d("Karina receiver","Student Created...Action: " + intent.getAction());
    }
}
