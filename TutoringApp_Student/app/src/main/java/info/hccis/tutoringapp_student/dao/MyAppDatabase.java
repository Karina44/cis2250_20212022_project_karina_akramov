package info.hccis.tutoringapp_student.dao;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import info.hccis.tutoringapp_student.entity.Student;

@Database(entities = {Student.class},version = 1)
public abstract class MyAppDatabase extends RoomDatabase {

    public abstract StudentDAO studentDAO();
}

