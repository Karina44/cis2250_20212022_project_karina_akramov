package info.hccis.tutoringapp_student.entity;


import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import info.hccis.tutoringapp_student.MainActivity;
import info.hccis.tutoringapp_student.R;
import info.hccis.tutoringapp_student.ViewStudentFragment;
import info.hccis.tutoringapp_student.api.ApiWatcher;
import info.hccis.tutoringapp_student.api.JsonStudentApi;
import info.hccis.tutoringapp_student.util.NotificationUtil;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
public class StudentContent {

    /**
     * This method will take the list passed in and reload the room database
     * based on the items in the list.
     * @param students
     * @since 20220210
     * @author Karina
     */
    public static void reloadStudentsInRoom(List<Student> students)
    {
        MainActivity.getMyAppDatabase().studentDAO().deleteAll();
        for(Student current : students)
        {
            MainActivity.getMyAppDatabase().studentDAO().insert(current);
        }
        Log.d("Karina Room","loading students into Room");
    }


    /**
     * This method will obtain all the ticket orders out of the Room database.
     * @return list of ticket orders
     * @since 20220210
     * @author Karina
     */
    public static List<Student> getStudentFromRoom()
    {
        Log.d("Karina Room","Loading Student from Room");

        List<Student> studentBack = MainActivity.getMyAppDatabase().studentDAO().selectAllStudents();
        Log.d("Karina Room","Number of Students loaded from Room: " + studentBack.size());
        for(Student current : studentBack)
        {
            Log.d("Karina Room",current.toString());
        }
        return studentBack;
    }
}
