package info.hccis.tutoringapp_student.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

import info.hccis.tutoringapp_student.entity.Student;

@Dao
public interface StudentDAO {

    @Insert
    void insert(Student student);

    @Query("SELECT * FROM student")
    List<Student> selectAllStudents();

    @Query("delete from student")
    public void deleteAll();

}
