package info.hccis.tutoringapp_student;
import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.navigation.fragment.NavHostFragment;

import com.google.android.material.snackbar.Snackbar;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;
import androidx.fragment.app.Fragment;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


import info.hccis.tutoringapp_student.entity.Student;
import info.hccis.tutoringapp_student.entity.StudentRepository;
import info.hccis.tutoringapp_student.entity.StudentViewModel;
import info.hccis.tutoringapp_student.databinding.FragmentAddStudentBinding;
import info.hccis.tutoringapp_student.util.ContentProviderUtil;

public class AddStudentFragment extends Fragment {

    public static final String KEY = "info.hccis.tutoring.student";
    private FragmentAddStudentBinding binding;
    private Student student;
    private StudentViewModel studentViewModel;
    private StudentRepository studentRepository;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("AddStudentFragment Karina", "onCreate triggered");
    }

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {

        Log.d("AddStudentFragment Karina", "onCreateView triggered");
        binding = FragmentAddStudentBinding.inflate(inflater, container, false);
        return binding.getRoot();

    }


    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Log.d("AddStudentFragment Karina", "onViewCreated triggered");

        studentViewModel = new ViewModelProvider(getActivity()).get(StudentViewModel.class);

        //******************************************************************************
        // Content Providers
        // Check permission for user to use the calendar provider.
        //******************************************************************************

        final int callbackId = 42;
        ContentProviderUtil.checkPermission(getActivity(), callbackId, Manifest.permission.READ_CALENDAR, Manifest.permission.WRITE_CALENDAR);

        studentRepository = studentRepository.getInstance();
        //************************************************************************************
        // Add a listener on the button.
        //************************************************************************************

        binding.buttonAddStudent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Log.d("AddStudentFragment Karina", "Calculate was clicked");

                try {
                    //************************************************************************************
                    // Call the calculate method
                    //************************************************************************************

                    addStudent();

                    //************************************************************************************
                    // I have made the TicketOrderBO implement Serializable.  This will allow us to
                    // serialize the object and then it can be passed in the bundle.  Note that the
                    // calculate method also sets the
                    Bundle bundle = new Bundle();
                    bundle.putSerializable(KEY, student);

                    //************************************************************************************
                    // See the view model research component.  The ViewModel is associated with the Activity
                    // and the add and view fragments share the same activity.  Am using the view model
                    // to hold the list of TicketOrderBO objects.  This will allow both fragments to
                    // access the list.
                    //************************************************************************************

                    //******************************************************************************
                    // Handle the new ticket order
                    // Use the post service to add the new ticket to the web database.
                    //******************************************************************************

                    //ticketOrderViewModel.getTicketOrders().add(ticketOrder);
                    studentRepository.getStudentService().createStudent(student).enqueue(new Callback<Student>() {
                        @Override
                        public void onResponse(Call<Student> call, Response<Student> r) {
                            Toast.makeText(getContext(), "Student is successfully added!", Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        public void onFailure(Call<Student> call, Throwable t) {
                            Toast.makeText(getContext(), "Error Adding Student: " + t.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    });


                    //******************************************************************************
                    //Activity for Thursday.
                    //Call the post rest api web service to have the new ticket order added to the database.
                    //******************************************************************************

                    Snackbar.make(view, "Have added a Student.  Want to add this new Student to the database. " +
                            "How should we handle this?", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();

                    //******************************************************************************
                    //Create the event without using Calendar intent
                    //******************************************************************************

                    long eventID = ContentProviderUtil.createEvent(getActivity(), student.toString());
                    Toast.makeText(getActivity(), "Calendar Event Created (" + eventID + ")", Toast.LENGTH_SHORT);
                    Log.d("Karina Calendar", "Calendar Event Created (" + eventID + ")");

                    //Using an intent
//                    ContentProviderUtil.createCalendarEventIntent(getActivity(), ticketOrder.toString());


                    //******************************************************************************
                    // Send a broadcast.
                    //******************************************************************************

                    //Send a broadcast.
                    LocalBroadcastManager lbm = LocalBroadcastManager.getInstance(view.getContext());
                    Intent intent = new Intent();
                    intent.setAction("info.hccis.tutoring.student");
                    lbm.sendBroadcast(intent);
                    getActivity().sendBroadcast(intent);


                    //************************************************************************************
                    // Navigate to the view orders fragment.  Note that the navigation is defined in
                    // the nav_graph.xml file.
                    //************************************************************************************

                    NavHostFragment.findNavController(AddStudentFragment.this)
                            .navigate(R.id.action_AddStudentFragment_to_ViewStudentFragment, bundle);
                } catch (Exception e) {
                    //************************************************************************************
                    // If there was an exception thrown in the calculate method, just put a message
                    // in the Logcat and leave the user on the add fragment.
                    //************************************************************************************
                    Log.d("AddStudentFragment Karina", "Error calculating: " + e.getMessage());
                }
            }
        });

    }

    /**
     * calculate the ticket cost based on the controls on the view.
     *
     * @throws Exception Throw exception if number of tickets entered caused an issue.
     * @author CIS2250
     * @since 20220118
     */
    public void addStudent() throws Exception {

        Log.d("Karina-MainActivity", "First Name entered =" + binding.editTextFirstName.getText().toString());
        Log.d("Karina-MainActivity", "Last Name entered =" + binding.editTextLastName.getText().toString());
        Log.d("Karina-MainActivity", "Phone Number entered =" + binding.editTextPhoneNumber.getText().toString());
        Log.d("Karina-MainActivity", "Email Address entered =" + binding.editTextEmail.getText().toString());
        Log.d("Karina-MainActivity", "Program Code entered =" + binding.editTextProgramCode.getText().toString());


        student = new Student();
        student.setFirstName(binding.editTextFirstName.getText().toString());
        student.setLastName(binding.editTextLastName.getText().toString());
        student.setPhoneNumber(binding.editTextPhoneNumber.getText().toString());
        student.setEmailAddress(binding.editTextEmail.getText().toString());
        int programCode;
        try {
            programCode = Integer.parseInt(binding.editTextProgramCode.getText().toString());
        } catch (Exception e) {
            programCode = 1;
        }
        student.setProgramCode(programCode);
        //ADD TICKET ORDER ORDER TO THE REALTIME DATABASE
        // CREATING A NEW INSTANCE OF DBASE
        DBStudent dbase= new DBStudent();
        dbase.addStudentDatabase(student);

    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}
