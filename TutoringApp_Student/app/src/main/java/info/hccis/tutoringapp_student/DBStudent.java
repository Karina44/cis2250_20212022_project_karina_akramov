package info.hccis.tutoringapp_student;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import info.hccis.tutoringapp_student.entity.Student;

public class DBStudent {
    private DatabaseReference databaseReference;

    public DBStudent() {
        FirebaseDatabase db = FirebaseDatabase.getInstance();
        databaseReference = db.getReference(Student.class.getSimpleName());
    }

    public void addStudentDatabase(Student student) {
        databaseReference.push().setValue(student);
    }
}
