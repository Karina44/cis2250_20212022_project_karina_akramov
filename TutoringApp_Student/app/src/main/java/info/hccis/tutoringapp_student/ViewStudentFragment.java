package info.hccis.tutoringapp_student;

import androidx.fragment.app.Fragment;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import info.hccis.tutoringapp_student.adapter.CustomAdapterStudent;
import info.hccis.tutoringapp_student.databinding.FragmentViewStudentsBinding;
import info.hccis.tutoringapp_student.entity.Student;
import info.hccis.tutoringapp_student.entity.StudentViewModel;
import info.hccis.tutoringapp_student.util.NotificationApplication;
import info.hccis.tutoringapp_student.util.NotificationUtil;


public class ViewStudentFragment extends Fragment {

    private static Context context;
    private FragmentViewStudentsBinding binding;
    private List<Student> studentArrayList;
    private static RecyclerView recyclerView;
    public static RecyclerView getRecyclerView() {
        return recyclerView;
    }

    public static void notifyDataChanged(String message){
        Log.d("Karina", "Data changed:  "+message);
        //Send a notification that the data has changed.
        try {
            recyclerView.getAdapter().notifyDataSetChanged();
        }catch(Exception e){
            Log.d("Karina api","Exception when trying to notify that the data set as changed");
        }
    }

    /**
     * Provide notification tha the data has changed.  This method will notify the adapter that the
     * rows have changed so it will know to refresh.  It will also send a notification to the user which
     * will allow them to go directly back to the list from another activity of the app.
     * @param message Message to display
     * @param activity - originating activity
     * @param destinationClass - class associated with the intent associated with the notification.
     */
    public static void notifyDataChanged(String message, Activity activity, Class destinationClass){
        Log.d("Karina", "Data changed:  "+message);
        try {
            notifyDataChanged(message);
            NotificationApplication.setContext(context);
            NotificationUtil.sendNotification("Tutoring Data Update", message, activity, MainActivity.class);
        }catch(Exception e){
            Log.d("Karina notification", "Exception occured when notifying. "+e.getMessage());
        }



    }


    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {

        binding = FragmentViewStudentsBinding.inflate(inflater, container, false);
        return binding.getRoot();

    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //************************************************************************************
        // Corresponding to the add fragment, the view model is accessed to obtain a reference
        // to the list of ticket order bo objects.
        //************************************************************************************
        StudentViewModel studentViewModel = new ViewModelProvider(getActivity()).get(StudentViewModel.class);

        //************************************************************************************
        //NO LONGER USED BUT LEFT AS EXAMPLE
        //Bundle is accessed to get the ticket order which is passed from the add order fragment.
        //************************************************************************************
        //        Bundle bundle = getArguments(); //Note not doing anything...here for example
        //        TicketOrder ticketOrder = (TicketOrder) bundle.getSerializable(AddOrderFragment.KEY);
        //        Log.d("ViewOrdersFragment Karina", "Ticket passed in:  " + ticketOrder.toString());

        //************************************************************************************
        //Build the output to be displayed in the textview
        // NOTE:  This output string is no longer displayed since we added the RecyclerView
        //************************************************************************************

        String output = "";

        for (Student stud : studentViewModel.getStudents()) {
            output += stud.toString() + System.lineSeparator();
        }

        //************************************************************************************
        // Set the context to be used when sending notifications
        //************************************************************************************

        context = getView().getContext();

        //************************************************************************************
        // Setup the recycler view for displaying the items in the ticket order list.
        //************************************************************************************

        recyclerView = binding.recyclerView;

        studentArrayList = studentViewModel.getStudents();
        setAdapter();

        //************************************************************************************
        // Button sends the user back to the add fragment
        //************************************************************************************

        binding.buttonAddStudent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavHostFragment.findNavController(ViewStudentFragment.this)
                        .navigate(R.id.action_ViewStudentFragment_to_AddStudentFragment);
            }
        });
    }

    /**
     * Set the adapter for the recyclerview
     * @since 20220129
     * @author Karina
     */
    private void setAdapter() {
        CustomAdapterStudent adapter = new CustomAdapterStudent(studentArrayList);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

}
